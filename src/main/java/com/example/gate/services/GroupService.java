package com.example.gate.services;

import com.example.gate.DTO.GroupDTO;
import com.example.gate.DTO.UserDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Service
public class GroupService {

    @Value("${user-service.url}")
    private String USER_SERVICE_URI;

//    private final String USER_SERVICE_URI = "http://195.58.49.179:8234";

    public GroupDTO[] getGroupList (String accessToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + accessToken);

        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity httpEntity = new HttpEntity(headers);
        RestTemplate restTemplate = new RestTemplate();
        GroupDTO[] groupList = restTemplate.exchange(USER_SERVICE_URI + "/api/v1/groups", HttpMethod.GET, httpEntity, GroupDTO[].class).getBody();
        return groupList;
    }

    public Set<String> getUsersIdByGroup (String accessToken, Long id) {
        HttpHeaders headers = getHeaders(accessToken);
        HttpEntity httpEntity = new HttpEntity(headers);
        RestTemplate restTemplate = new RestTemplate();
        UserDTO[] userList = restTemplate.exchange(USER_SERVICE_URI + "/api/v1/groups/" + id + "/users", HttpMethod.GET,
                                                   httpEntity, UserDTO[].class).getBody();
        Set<String> usersId =  new HashSet<>();
        for (UserDTO user: userList) {
            usersId.add(user.getId());
        }
        return usersId;
    }

    private static HttpHeaders getHeaders (String accessToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + accessToken);

        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        return headers;
    }

    public String getUSER_SERVICE_URI() {
        return USER_SERVICE_URI;
    }
}
