package com.example.gate.services;

import com.example.gate.DTO.StatusDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class StatusService {

    private final WebClient webClient;

    @Autowired
    public StatusService(WebClient webClient) {
        this.webClient = webClient;
    }


    public StatusDTO[] getAllStatus(String accessToken) {
        StatusDTO[] response = webClient.get().uri("/status")
                .header("Authorization", "Bearer " + accessToken.toString())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(StatusDTO[].class)
                .block();
        return response;
    }


    public StatusDTO getStatusById(Long id, String accessToken) {

        StatusDTO response = webClient.get().uri("/status/" + id)
                .header("Authorization", "Bearer " + accessToken.toString())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(StatusDTO.class)
                .block();
        return response;
    }
    public StatusDTO getStatusByName(String name, String accessToken) {
        StatusDTO response = webClient.get().uri("/status/name=" + name)
                .header("Authorization", "Bearer " + accessToken.toString())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(StatusDTO.class)
                .block();
        return response;
    }
}
