package com.example.gate.services;

import com.example.gate.DTO.ComponentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class ComponentService {

    final String uri = "http://localhost:9090/components";

    private final WebClient webClient;

    @Autowired
    public ComponentService(WebClient webClient) {
        this.webClient = webClient;
    }

    public ComponentDTO[] getAllComponent(String accessToken) {
//        RestTemplate restTemplate = new RestTemplate();
//        ResponseEntity<ComponentDTO[]> componentDTOResponseEntity = restTemplate.getForEntity(uri,ComponentDTO[].class);
//        ComponentDTO[] componentDTOS = componentDTOResponseEntity.getBody();
//        return componentDTOS;
        ComponentDTO[] response = webClient.get().uri("/components" )
                .header("Authorization", "Bearer " + accessToken.toString())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(ComponentDTO[].class)
                .block();
        return response;
    }

    public ComponentDTO getComponentById(Long id, String accessToken) {
        ComponentDTO response = webClient.get().uri("/components/" + id)
                .header("Authorization", "Bearer " + accessToken.toString())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(ComponentDTO.class)
                .block();
        return response;
    }

    public ComponentDTO getComponentByName(String name, String accessToken) {
        ComponentDTO response = webClient.get().uri("/components/name=" + name)
                .header("Authorization", "Bearer " + accessToken.toString())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(ComponentDTO.class)
                .block();
        return response;

    }
}
