package com.example.gate.services;


import com.example.gate.DTO.CommentResponseDTO;
import com.example.gate.DTO.CommentUserDTO;
import com.example.gate.DTO.StatusDTO;
import com.example.gate.DTO.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

@Service
public class CommentService {

    private final WebClient webClient;
    private final UserService userService;
    private final StatusService statusService;

    @Autowired
    public CommentService(WebClient webClient, UserService userService, StatusService statusService) {
        this.webClient = webClient;
        this.userService = userService;
        this.statusService = statusService;
    }

    public void saveComment (Long id, String accessToken, CommentResponseDTO commentResponseDTO) {
        webClient.post().uri("/tasks/" + id + "/comments")
                .header("Authorization", "Bearer " + accessToken.toString())
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(commentResponseDTO), CommentResponseDTO.class)
                .retrieve()
                .bodyToMono(CommentResponseDTO.class).block();
    }

    public CommentResponseDTO[] getAllCommentsForTask(Long id, String accessToken) {
        CommentResponseDTO[] response = webClient.get().uri("/comments/" + id)
                .header("Authorization", "Bearer " + accessToken.toString())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(CommentResponseDTO[].class)
                .block();
        return response;
    }

    public CommentUserDTO[] getCommentsWithUsers (Long id, String accessToken, String userId) {
        CommentResponseDTO[] commentResponseDTOS = getAllCommentsForTask(id, accessToken);
        CommentUserDTO[] commentUserDTOS = new CommentUserDTO[commentResponseDTOS.length];
        for (int i = 0; i < commentResponseDTOS.length; i++) {
            CommentUserDTO commentUserDTO = new CommentUserDTO();
            commentUserDTO.setId(commentResponseDTOS[i].getId());
            commentUserDTO.setText(commentResponseDTOS[i].getText());
            commentUserDTO.setTaskId(commentResponseDTOS[i].getTask());
            commentUserDTO.setSendDate(commentResponseDTOS[i].getSendDate());
            commentUserDTO.setSenderId(commentResponseDTOS[i].getSenderId());
            UserDTO userDTO = userService.getUser(commentResponseDTOS[i].getSenderId(), accessToken);
            commentUserDTO.setSenderName(userDTO.getFirstName() + " " + userDTO.getLastName());
            commentUserDTOS[i] = commentUserDTO;
            System.out.println(commentUserDTO.toString());
            System.out.println(userDTO.toString());

        }
        return  commentUserDTOS;
    }

    public void createStatusChangeComment (Long id, String accessToken, Object status, String userId) {
        StatusDTO statusDTO = statusService.getStatusById(Long.parseLong((String)status), accessToken);
        UserDTO userDTO = userService.getUser(userId, accessToken);
        CommentResponseDTO commentResponseDTO = new CommentResponseDTO();
        commentResponseDTO.setSenderId(userId);
        commentResponseDTO.setSendDate(LocalDateTime.now());
        commentResponseDTO.setTask(id);
        commentResponseDTO.setText(userDTO.getFirstName() + " " + userDTO.getLastName() +
                " изменил статус на '" + statusDTO.getName() + "'.");
        this.saveComment(id, accessToken, commentResponseDTO);
    }



}
