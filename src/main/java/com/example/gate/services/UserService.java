package com.example.gate.services;

import com.example.gate.DTO.GroupDTO;
import com.example.gate.DTO.UserDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Service
public class UserService {

    @Value("${user-service.url}")
    private String USER_SERVICE_URI;

    public UserDTO getUser(String id, String accessToken) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", "Bearer " + accessToken);

            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

            HttpEntity httpEntity = new HttpEntity(headers);
            RestTemplate restTemplate = new RestTemplate();
            UserDTO userDTO = restTemplate.exchange(USER_SERVICE_URI + "/api/v1/users/" + id, HttpMethod.GET, httpEntity, UserDTO.class).getBody();
            return userDTO;
        } catch (HttpClientErrorException e) {
            return new UserDTO();
        }
    }

    public UserDTO getUserRoles(String id, String accessToken) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", "Bearer " + accessToken);

            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

            HttpEntity httpEntity = new HttpEntity(headers);
            RestTemplate restTemplate = new RestTemplate();
            UserDTO userDTO = restTemplate.exchange(USER_SERVICE_URI + "/api/v1/users/" + id, HttpMethod.GET, httpEntity, UserDTO.class).getBody();
            return userDTO;
        } catch (HttpClientErrorException e) {
            return new UserDTO();
        }
    }


}
