package com.example.gate.services;

import com.example.gate.DTO.ComponentDTO;
import com.example.gate.DTO.TaskDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;

@Service
public class TaskService {

    private final WebClient webClient;
    private final ComponentService componentService;

    @Autowired
    public TaskService(WebClient webClient, ComponentService componentService) {
        this.webClient = webClient;
        this.componentService = componentService;
    }

    public TaskDTO[] getAllTasks(String accessToken) {
        TaskDTO[] response = webClient.get().uri("/tasks")
                .header("Authorization", "Bearer " + accessToken.toString())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(TaskDTO[].class)
                .block();
        return response;
    }

    public void shareTask (String accessToken, Long id, Set<String> users) {
        webClient.post().uri("/tasks/" +id + "/groups").header("Authorization", "Bearer " + accessToken.toString())
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(users), Set.class)
                .retrieve()
                .bodyToMono(TaskDTO.class).block();
    }
    public void deleteTask (String accessToken, Long id) {
        webClient.delete().uri("/tasks/" + id).header("Authorization", "Bearer " + accessToken.toString())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(TaskDTO.class)
                .block();
    }

    public TaskDTO[] getAllTasksByCreatorId(String accessToken, String userId) {
        try {
        TaskDTO[] response = webClient.get().uri("/tasks?param=creator&user=" + userId)
                .header("Authorization", "Bearer " + accessToken.toString())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(TaskDTO[].class)
                .block();
                return response;
        } catch (WebClientResponseException exception) {
            return new TaskDTO[0];
        }
    }

    public TaskDTO[] getAllTasksByAssignerId(String accessToken, String userId) {
        try {
            TaskDTO[] response = webClient.get().uri("/tasks?param=assign&user=" + userId)
                    .header("Authorization", "Bearer " + accessToken.toString())
                    .accept(MediaType.APPLICATION_JSON)
                    .retrieve()
                    .bodyToMono(TaskDTO[].class)
                    .block();
            return response;
        } catch (WebClientResponseException exception) {
            return new TaskDTO[0];
        }
    }

    public TaskDTO getTaskById(Long id, String accessToken) {
        TaskDTO response = webClient.get().uri("/tasks/" + id)
                .header("Authorization", "Bearer " + accessToken.toString())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(TaskDTO.class)
                .block();
        return response;
    }

    public TaskDTO updateTask(Long id, Map<String, Object> updates, String accessToken) {
        return webClient.patch().uri("tasks/" + id)
                .header("Authorization", "Bearer " + accessToken.toString())
                .bodyValue(updates)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(TaskDTO.class).block();
    }

    public TaskDTO saveTask(TaskDTO taskDTO, String accessToken) {
        return webClient.put().uri("/tasks/" + taskDTO.getId())
                .header("Authorization", "Bearer " + accessToken.toString())
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(taskDTO), TaskDTO.class)
                .retrieve()
                .bodyToMono(TaskDTO.class).block();
    }

    public TaskDTO createTask(TaskDTO taskDTO, String accessToken, String component, String userId, Set<String> roles) {

        ComponentDTO componentDTO = componentService.getComponentByName(component, accessToken);
        taskDTO.setComponentId(componentDTO.getId());
        taskDTO.setStatusId(Integer.toUnsignedLong(1));
        taskDTO.setTaskCreatorId(userId);
        System.out.println(componentDTO.toString());
        if (roles.contains("ROLE_student")) {
            taskDTO.setAssignedToId(userId);
        }
        if (componentDTO.getName().equals("Общежитие")) {
            taskDTO.setAssignedToId(componentDTO.getAutoAssignId());
        }
        return webClient.post().uri("/tasks")
                .header("Authorization", "Bearer " + accessToken.toString())
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(taskDTO), TaskDTO.class)
                .retrieve()
                .bodyToMono(TaskDTO.class).block();
    }

}
