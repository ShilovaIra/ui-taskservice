package com.example.gate.controllers;

import com.example.gate.DTO.*;
import com.example.gate.services.*;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;

@Controller
@RequestMapping("tasks")
public class TaskController {

    private final TaskService taskService;
    private final StatusService statusService;
    private final ComponentService componentService;
    private final CommentService commentService;
    private final GroupService groupService;
    private final HttpServletRequest request;
    private final UserService userService;
    private String token;
    private String userId;
    private Set<String> roles;
    private String login;

    @Autowired
    public TaskController(TaskService taskService, StatusService statusService,
                          ComponentService componentService, CommentService commentService,
                          GroupService groupService, HttpServletRequest request, UserService userService) {
        this.taskService = taskService;
        this.statusService = statusService;
        this.componentService = componentService;
        this.commentService = commentService;
        this.groupService = groupService;
        this.request = request;
        this.userService = userService;
    }

    @GetMapping()
    public String getTasks(Model model) {
        getToken();
        System.out.println(token);
        System.out.println(userId);
        for (String role: roles) {
            System.out.println(role);
        }
        login = userService.getUser(userId, token).getLogin();
        System.out.println( userService.getUser(userId, token).toString());
        TaskDTO[] creations = taskService.getAllTasksByCreatorId(token,userId);
        TaskDTO[] assigners = taskService.getAllTasksByAssignerId(token,userId);
        StatusDTO[] statusDTO = statusService.getAllStatus(token);
        model.addAttribute("taskCreate", creations);
        model.addAttribute("taskAssign", assigners);
        model.addAttribute("statusList", statusDTO);
        return "taskPage";
    }

    @GetMapping("/{id}")
    public String getTaskByID(@PathVariable(name = "id") Long id, Model model) {
        TaskDTO taskDTO = taskService.getTaskById(id, token);
        boolean isTeacher = false;
        boolean isMain = false;
        if (taskDTO.getTaskCreatorId().equals(userId)) {
            isMain = true;
        }
        if (roles.contains("ROLE_teacher") || login.contains("teacher")) {
            isTeacher = true;
        }
        GroupDTO[] groupDTOS = groupService.getGroupList(token);
        StatusDTO statusDTO = statusService.getStatusById(taskDTO.getStatusId(), token);
        StatusDTO[] statusList = statusService.getAllStatus(token);
        ComponentDTO componentDTO = componentService.getComponentById(taskDTO.getComponentId(), token);
        CommentResponseDTO[] comments = commentService.getAllCommentsForTask(taskDTO.getId(), token);
        StatusDTO status = statusService.getStatusById(taskDTO.getStatusId(), token);
        CommentUserDTO[] commentUserDTOS = commentService.getCommentsWithUsers(id, token, userId);
        UserDTO creator = userService.getUser(taskDTO.getTaskCreatorId(), token);
        model.addAttribute("role", isTeacher);
        model.addAttribute("groups", groupDTOS);
        model.addAttribute("delete", isMain);
        model.addAttribute("status", status);
        model.addAttribute("statusList", statusList);
        model.addAttribute("task", taskDTO);
        model.addAttribute("statusDTO", statusDTO);
        model.addAttribute("comment", comments);
        model.addAttribute("senders", commentUserDTOS);
        model.addAttribute("componentDTO", componentDTO);
        model.addAttribute("creator", creator);

        return "taskInformationPage";
    }

    @PostMapping(value = "{id}")
    public String partitionUpdate(@PathVariable(name = "id") Long id,
                                  @RequestParam Map<String, Object> updates,
                                  Model model) {
        TaskDTO updated = taskService.updateTask(id, updates, token);
        if (updates.containsKey("statusId")) {
            Object value = updates.remove("statusId");
            commentService.createStatusChangeComment(id, token, value, userId);
        }
        return "redirect:/tasks/" + id;
    }

    @RequestMapping(value = "/{id}/delete", method = RequestMethod.POST)
    public String deleteTask (@PathVariable(name = "id") Long id) {
        taskService.deleteTask(token,id);
        return "redirect:/tasks";
    }

    @RequestMapping(value = "/{id}/comment", method = RequestMethod.POST)
    public String createNewComment(@PathVariable(name = "id") Long id,
                                   @RequestParam(value = "comment") String text) {
        CommentResponseDTO commentResponseDTO = new CommentResponseDTO();
        commentResponseDTO.setSenderId(userId);
        commentResponseDTO.setTask(id);
        commentResponseDTO.setText(text);
        commentResponseDTO.setSendDate(LocalDateTime.now());
        commentService.saveComment(id, token, commentResponseDTO);
        return "redirect:/tasks/" + id;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String createNewTask(Model model, @RequestParam(value = "taskName") String name,
                                @RequestParam(value = "taskDescription") String description,
                                @RequestParam(value = "taskStartDate") String taskStartDate,
                                @RequestParam(value = "taskEndDate") String taskEndDate,
                                @RequestParam(value = "component") String component) {

        TaskDTO taskDTO = new TaskDTO(name, description, LocalDateTime.parse(taskStartDate),
                                      LocalDateTime.parse(taskEndDate), LocalDateTime.now());
        TaskDTO taskDTO1 = taskService.createTask(taskDTO, token, component, userId, roles);
        return "redirect:/tasks";
    }

    @GetMapping("/create")
    public String createNewTaskLink(Model model) {
        TaskDTO taskDTO = new TaskDTO();
        ComponentDTO[] componentDTOS = componentService.getAllComponent(token);
        Boolean isTeacher = false;
        model.addAttribute("componentList", componentDTOS);
        model.addAttribute("task", taskDTO);
        if (roles.contains("ROLE_teacher")) {
            isTeacher = true;
        }
        model.addAttribute("role", isTeacher);
        return "taskCreation";
    }

    @RequestMapping(value="/{id}/groups", method = RequestMethod.POST)
    public String shareTask(@PathVariable(name = "id") Long id,
                            @RequestParam Long groupId) {
        Set<String> users = groupService.getUsersIdByGroup(token, groupId);
        taskService.shareTask(token, id, users);
        return "redirect:/tasks/" + id;
    }

    @GetMapping("/logout")
    public String logout() throws ServletException {
        request.logout();
        return "redirect:";
    }

    private String getToken() {
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        token = context.getTokenString();
        AccessToken contextToken = context.getToken();
        userId = contextToken.getSubject();
        roles = contextToken.getRealmAccess().getRoles();
        return userId;
    }
}
