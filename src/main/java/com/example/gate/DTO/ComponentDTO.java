package com.example.gate.DTO;

import java.util.Set;

public class ComponentDTO {

    private Long id;
    private String name;
    private String autoAssignId;
    private Set<Long> taskIds;

    public ComponentDTO() {
    }

    public ComponentDTO(Long id, String name, String autoAssignId, Set<Long> taskIds) {
        this.id = id;
        this.name = name;
        this.autoAssignId = autoAssignId;
        this.taskIds = taskIds;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAutoAssignId() {
        return autoAssignId;
    }

    public void setAutoAssignId(String autoAssignId) {
        this.autoAssignId = autoAssignId;
    }

    public Set<Long> getTaskIds() {
        return taskIds;
    }

    public void setTaskIds(Set<Long> taskIds) {
        this.taskIds = taskIds;
    }

    @Override
    public String toString() {
        return "ComponentDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", autoAssignId=" + autoAssignId +
                '}';
    }
}
