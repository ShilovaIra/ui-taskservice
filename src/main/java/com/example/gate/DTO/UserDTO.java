package com.example.gate.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class UserDTO {

    @JsonProperty("id")
    private String id;

    @JsonProperty("first_name")
    @NotBlank(message="Blank first name field.")
    private String firstName;

    @JsonProperty("last_name")
    @NotBlank(message="Blank last name field.")
    private String lastName;

    @JsonProperty("email")
    @NotBlank(message="Blank email field.")
    private String email;

    @JsonProperty("birthdate")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    @JsonProperty("login")
    @NotBlank(message="Blank login field.")
    private String login;

    @JsonProperty("course_speciality")
    private List<RoleDepartmentDto> roleDepartmentDtoList;

    @JsonProperty("role_department")
    private List<CourseSpecialityDto> courseSpecialityDtoList;

    public UserDTO() {
    }

    public UserDTO(String id, String firstName, String lastName, String email, Date birthday, String login,
                   List<RoleDepartmentDto> roleDepartmentDtoList,
                   List<CourseSpecialityDto> courseSpecialityDtoList) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.birthday = birthday;
        this.login = login;
        this.roleDepartmentDtoList = roleDepartmentDtoList;
        this.courseSpecialityDtoList = courseSpecialityDtoList;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", birthday=" + birthday +
                ", login='" + login + '\'' +
                ", roleDepartmentDtoList=" + roleDepartmentDtoList +
                ", courseSpecialityDtoList=" + courseSpecialityDtoList +
                '}';
    }
}
