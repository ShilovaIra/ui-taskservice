package com.example.gate.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RoleDepartmentDto {

    @JsonProperty("role_id")
    @NotNull(message = "Blank role field.")
    private Long roleId;
    @JsonProperty("department_id")
    @NotNull(message = "Blank department field.")
    private Long departmentId;

}
