package com.example.gate.DTO;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class CommentUserDTO {

    private Long id;
    private String text;
    private String senderId;
    private String senderName;
    private LocalDateTime sendDate;
    private Long taskId;

    public CommentUserDTO() {
    }

    public CommentUserDTO(Long id, String text, String senderId, String senderName, LocalDateTime sendDate, Long taskId) {
        this.id = id;
        this.text = text;
        this.senderId = senderId;
        this.senderName = senderName;
        this.sendDate = sendDate;
        this.taskId = taskId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public LocalDateTime getSendDate() {
        return sendDate;
    }

    public LocalDate getSendLocalDate() {
        return sendDate.toLocalDate();
    }


    public void setSendDate(LocalDateTime sendDate) {
        this.sendDate = sendDate;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    @Override
    public String toString() {
        return "CommentUserDTO{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", senderId='" + senderId + '\'' +
                ", senderName='" + senderName + '\'' +
                ", sendDate=" + sendDate +
                ", taskId=" + taskId +
                '}';
    }
}
