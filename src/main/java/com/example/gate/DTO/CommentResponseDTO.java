package com.example.gate.DTO;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class CommentResponseDTO {

    private Long id;
    private String text;
    private String senderId;
    private LocalDateTime sendDate;
    private Long taskId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public LocalDateTime getSendDate() {
        return sendDate;
    }

    public LocalDate getSendLocalDate() {
        return sendDate.toLocalDate();
    }

    public void setSendDate(LocalDateTime sendDate) {
        this.sendDate = sendDate;
    }

    public Long getTask() {
        return taskId;
    }

    public void setTask(Long taskId) {
        this.taskId = taskId;
    }
}
