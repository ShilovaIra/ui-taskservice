package com.example.gate.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CourseSpecialityDto {

    @JsonProperty("course_value")
    private byte courseValue;
    @JsonProperty("speciality_id")
    private long specialityId;

}
