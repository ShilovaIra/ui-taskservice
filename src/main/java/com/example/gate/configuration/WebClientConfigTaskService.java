package com.example.gate.configuration;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.concurrent.TimeUnit;
import reactor.netty.http.client.HttpClient;
import reactor.netty.tcp.TcpClient;


@Configuration
public class WebClientConfigTaskService {

    @Value("${task-service.url}")
    private String TASK_SERVICE_URL;
    public static final int TIMEOUT = 5000;

    public TcpClient createTcpClient(int timeout){
        return TcpClient
                .create()
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, timeout)
                .doOnConnected(connection -> {
                    connection.addHandlerLast(new ReadTimeoutHandler(TIMEOUT, TimeUnit.MILLISECONDS));
                    connection.addHandlerLast(new WriteTimeoutHandler(TIMEOUT, TimeUnit.MILLISECONDS));
                });
    }

    @Bean(name="TaskServiceWebClient")
    public WebClient webClientWithTimeout() {
        return WebClient.builder()
                .baseUrl(TASK_SERVICE_URL)
                .clientConnector(new ReactorClientHttpConnector(HttpClient.from(createTcpClient(TIMEOUT))))
                .build();
    }
}

