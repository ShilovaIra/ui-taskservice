<#macro taskList>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>Welcome</title>
    <link rel="stylesheet" type="text/css" href="/css/baseStyle.css"/>
</head>
<body>
    <div class="simple-main-menu">
        <div class="simple-main-menu-logo">
            <label class="my-tasks-label"> Мои задачи: </label>
        </div>
        <div class="create-task-button-from-menu">
                <a href="/tasks/create" class="simple-main-menu-create-task"> Создать </a>
        </div>
    </div>
    <div class="main-task-list">
        <#list taskCreate as task>
            <div class="tasks-list-output">
                <a href="/tasks/${task.id}">
                    ${task.name}
                </a>
                <div class="taskList-end-date"> Выполнить до: ${task.getDate()} </div>
            </div>
        <#else>
            Нет задач
        </#list>
    </div>

    <div class="simple-main-menu">
        <div class="simple-main-menu-logo">
            <label class="my-tasks-label"> Назначены на меня: </label>
        </div>
    </div>
    <div class="main-task-list">
        <#list taskAssign as task>
            <div class="tasks-list-output">
                <a href="/tasks/${task.id}">
                    ${task.name}
                </a>
                <div class="taskList-end-date"> Выполнить до: ${task.getDate()} </div>
            </div>
        <#else>
            Нет задач
        </#list>
    </div>

</body>
</html>
</#macro>
