<#macro base_tasks>
    <!DOCTYPE HTML>
    <html lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>Welcome</title>
        <link rel="stylesheet" type="text/css" href="/css/baseStyle.css"/>
    </head>
    <body>
    <div class="navbar">
        <label class="navbar-main-logo"> <a href="/task" class="main-link"> Задачи </a> </label>
        <a href="/task/logout" class="logout-link"> Выйти </a>
    </div>
    <#nested>
    </body>
    </html>
</#macro>