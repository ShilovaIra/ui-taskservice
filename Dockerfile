FROM openjdk:11
VOLUME /tmp
RUN mkdir -p /uitm/
RUN mkdir -p /uitm/logs/
ADD /target/gate-0.0.1-SNAPSHOT.jar /uitm/uitm.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=container", "-jar", "/uitm/uitm.jar"]
